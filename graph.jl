type Edge
    u :: Int64 
    v :: Int64
    w :: Int64
end

type Graph
    edges :: Vector{Vector{Edge}}
    n :: Int64
    m :: Int64
    Graph(n :: Int64, m :: Int64) = new([ [] for i=1:n], n, m)
end

add_edge!(g :: Graph, u :: Int64, v :: Int64, w :: Int64) = push!(g.edges[u], Edge(u,v,w))

function get_edge(g :: Graph, u :: Int64, v :: Int64)
    for i = 1 : size(g.edges[u], 1)
        g.edges[u][i].v == v && return g.edges[u][i]
    end
end

function random_graph(n :: Int64, m :: Int64)
    g = Graph(n, m)
    for i = 1:m
        u = rand(1:n)
        v = rand(1:n)
        w = rand(1:100)
        while u == v
            v = rand(1:n)
        end
        add_edge!(g, u, v, w)
    end
    return g
end
