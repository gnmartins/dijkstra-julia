include("binary-heap.jl")
include("graph.jl")
include("dimacs-parser.jl")

using ArgParse

function dijkstra(src :: Int64, tgt :: Int64, g :: Graph, silent = false)
    tic()
    d = [ i != src? 2^63-1 : 0 for i=1:g.n ] # distances from src to nodes 
    visited = [ false for i=1:g.n ]          # visited nodes
    
    Q = Heap()
    nodesList = []
    for i = 1:g.n
        push!(nodesList, Node(i, -1))
    end 
    
    Q = insert!(Q, nodesList[src], 0)
    while size(Q.nodes, 1) != 0
        vNode = getmin(Q)
        v = vNode.e
        Q = delete_min!(Q)
        visited[v] = true
        for neighbor = 1 : size(g.edges[v], 1)
            neighborEdge = g.edges[v][neighbor]
            u = g.edges[v][neighbor].v
            if !visited[u]
                if d[u] == 2^63-1
                    d[u] = d[v] + neighborEdge.w
                    Q = insert!(Q, nodesList[u], d[u])
                elseif d[v] + neighborEdge.w < d[u]
                    d[u] = d[v] + neighborEdge.w
                    Q = decrease_key!(Q, nodesList[u], d[u])
                end
            end
        end
    end
    silent || (d[tgt] != 2^63-1? println(d[tgt]) : println("INF"))
    return [Q.swaps, Q.inserts, Q.updates, Q.deletes, toq()]
end

function parse_command_line()
    s = ArgParseSettings()
    @add_arg_table s begin
        "src"
            help = "source node"
            arg_type = Int64
            required = true
        "tgt"
            help = "target node"
            arg_type = Int64
            required = true
        "filename"
            help = "DIMACS file"
            required = true
        "--benchmark", "-b"
            help = "print elapsed time and memory usage"
            action = :store_true
        "--complexity-run", "-c"
            help = "execute with random graphs for analisys"
            action = :store_true
    end
    return parse_args(s)
end    

function main()
    parsed_args = parse_command_line()
    #for (arg, val) in parsed_args
    #    println("     $arg => $val")
    #end

    if parsed_args["benchmark"]
        println("reading input file")
        @time g = dimacs_to_graph(parsed_args["filename"])
        println("dijkstra run #0 (elapsed time includes compile time)")
        @time dijkstra(parsed_args["src"], parsed_args["tgt"], g)
        println("\n-----------------")
        println("benchmarking runs")
        println("-----------------")
        for i = 1:30
            @time dijkstra(parsed_args["src"], parsed_args["tgt"], g, true)
        end
        println("-----------------")
    else
        g = dimacs_to_graph(parsed_args["filename"])
        dijkstra(parsed_args["src"], parsed_args["tgt"], g)
    end
    return parsed_args
end

## main start
parsed_args = main()


#= dijkstra complexity test =#
if parsed_args["complexity-run"]
    n = 2^12
    println("---------- fixed |V| ----------")
    println("n\tm\tI\tU\tD\tT(s)")
    for i = 13:22
        m = 2^i
        I = []
        U = []
        D = []
        T = []
        g = random_graph(n, m)
        for k = 1:30
            _s, _i, _u, _d, _t = dijkstra(rand(1:n), rand(1:n), g, true)
            push!(I, _i)
            push!(U, _u)
            push!(D, _d)
            push!(T, _t)
        end
        meanI = trunc(mean(I), 4)
        meanU = trunc(mean(U), 4)
        meanD = trunc(mean(D), 4)
        meanT = trunc(mean(T), 4)
        println("$n\t$m\t$meanI\t$meanU\t$meanD\t$meanT")
    end

    println("-------------------------------")
    println("---------- fixed |E| ----------")
    m = 2^20
    println("n\tm\tI\tU\tD\tT(s)")
    for i = 11:20
        n = 2^i
        I = []
        U = []
        D = []
        T = []
        g = random_graph(n, m)
        for k = 1:30
            _s, _i, _u, _d, _t = dijkstra(rand(1:n), rand(1:n), g, true)
            push!(I, _i)
            push!(U, _u)
            push!(D, _d)
            push!(T, _t)
        end
        meanI = trunc(mean(I), 4)
        meanU = trunc(mean(U), 4)
        meanD = trunc(mean(D), 4)
        meanT = trunc(mean(T), 4)
        println("$n\t$m\t$meanI\t$meanU\t$meanD\t$meanT")
    end
end








    



