function dimacs_to_graph(filename :: String)
    g = Graph
    open(filename) do file
        line = readline(file)
        while contains(line, "p sp") == false
            line = readline(file)
        end
        n = parse(Int64, split(line)[3])
        m = parse(Int64, split(line)[4])
        g = Graph(n, m)

        counter = 0
        while counter < m
            line = readline(file)
            if !(line[1] == 'a')
                continue
            end
            u = parse(Int64, split(line)[2])
            v = parse(Int64, split(line)[3])
            w = parse(Int64, split(line)[4])
            #println("arco = $u -> $v = $w")
            add_edge!(g, u, v, w)
            counter += 1
        end
        return g
    end
    return g
end