include("binary-heap.jl")

function insert_test()
    println("---------- insert test ----------")
    n = 2^20-1
    i = 0
    inserts = 0
    last = 0
    expected = 0
    j = 0
    heap = Heap()
    tic()
    for j = 1:n
        if inserts == 2^i-1
            if i != 0
                expected = 2^(i-1) * (i-1)
                println("$inserts $(heap.swaps - last) $expected $(toq())")
            else
                println("$inserts $(heap.swaps - last) $expected $(toq())")
            end
            
            last = heap.swaps
            i += 1
            tic()
        end

        insert!(heap, Node(j,0), n-j)
        inserts += 1 
    end
    if inserts == 2^i-1
        expected = 2^(i-1) * (i-1)
        println("$inserts $(heap.swaps - last) $expected $(toq())")
        
        last = heap.swaps
        i += 1
    end 
    println("---------------------------------")
end

function update_test()
    println("---------- update test ----------")
    for i = 1:20
        n = 2^i-1 + 2^i
        heap = Heap(n)
        for k = 1:2^i-1
            insert!(heap, k, 2^i+1)
        end
        for k = 2^i:n
            insert!(heap, k, 2^i+2)
        end
        dec = 0
        tic()
        for k = 2^i:n
            update!(heap, k, 2^i-dec)
            dec += 1
        end
        println("$(2^i) $(heap.swaps) $(2^i * i) $(toq())")
    end
    println("---------------------------------")
end

function delete_test()
    println("---------- delete test ----------")
    for i = 1:20
        n = 2^i-1
        heap = Heap(n)
        for k = 1:n
            insert!(heap, k, rand(1:1000))
        end
        tic()
        for k = 1:n
            delete_min!(heap)
        end
        println("$(2^i-1) $(heap.swaps) $(2^i * log2(i-1)) $(toq())")
    end
    println("---------------------------------")
end

insert_test()
update_test()
delete_test()